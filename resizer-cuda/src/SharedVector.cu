#include "SharedVector.cuh"

struct Pixel {
    uchar r;
    uchar g;
    uchar b;
};
typedef struct Pixel Pixel;


template <class T>
SharedVector<T>::SharedVector(std::initializer_list<T> initVector) {
    this->size = initVector.size();

    this->vec = std::make_unique<T[]>(this->size);
    std::copy(initVector.begin(), initVector.end(), this->vec.get());

    this->byteSize = size * sizeof(T);

    this->device_vec = nullptr;
    cudaError_t mallocOut = cudaMalloc(&this->device_vec, this->byteSize);

    if (mallocOut != cudaSuccess) {
        std::cerr << "cudaMalloc failed!\n";
    }
}

template <class T>
SharedVector<T>::SharedVector(T* data, int elementCount) {
    this->size = elementCount;

    this->vec = std::make_unique<T[]>(this->size);
    std::copy(data, data + this->size, this->vec.get());

    this->byteSize = size * sizeof(T);

    this->device_vec = nullptr;
    cudaError_t mallocOut = cudaMalloc(&this->device_vec, this->byteSize);

    if (mallocOut != cudaSuccess) {
        std::cerr << "cudaMalloc failed!\n";
    }
}
template SharedVector<Pixel>::SharedVector(Pixel* data, int elementCount);

template <class T>
SharedVector<T>::SharedVector(int elementCount) {
    this->size = elementCount;

    this->vec = std::make_unique<T[]>(this->size);
    std::fill(this->vec.get(), this->vec.get() + this->size, T());

    this->byteSize = size * sizeof(T);

    this->device_vec = nullptr;
    cudaError_t mallocOut = cudaMalloc(&this->device_vec, this->byteSize);

    if (mallocOut != cudaSuccess) {
        std::cerr << "cudaMalloc failed!\n";
    }
}
template SharedVector<Pixel>::SharedVector(int elementCount);

template <class T>
void SharedVector<T>::print() {
    std::cout<<'{';
    for (uint i=0; i<this->size; ++i) {
        std::cout<<this->vec[i];
        if (i+1 < this->size)
            std::cout<<", ";
    }
    std::cout<<"}\n";
}

template <class T>
void SharedVector<T>::sendToGPU() {
    cudaError_t memcpyOut = cudaMemcpy(
        (void*) this->device_vec,
        (void*) this->vec.get(),
        this->byteSize, 
        cudaMemcpyHostToDevice);

    if (memcpyOut != cudaSuccess) {
        std::cerr << "cudaMemcpy (host to device) failed!\n";
    }
}
template void SharedVector<Pixel>::sendToGPU();

template <class T>
void SharedVector<T>::getFromGPU() {
    cudaError_t memcpyOut = cudaMemcpy(
        (void*) this->vec.get(),
        (void*) this->device_vec,
        this->byteSize, 
        cudaMemcpyDeviceToHost);

    if (memcpyOut != cudaSuccess) {
        std::cerr << "cudaMemcpy (device to host) failed!\n";
    }
}
template void SharedVector<Pixel>::getFromGPU();

template <class T>
SharedVector<T>::~SharedVector() {
    cudaError_t freeOut = cudaFree(this->device_vec);

    if (freeOut != cudaSuccess) {
        std::cerr << "cudaFree failed!\n";
    }
}

template SharedVector<Pixel>::~SharedVector();
