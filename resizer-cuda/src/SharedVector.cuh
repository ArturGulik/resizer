#ifndef SHAREDVECTOR_H
#define SHAREDVECTOR_H

#include <iostream>
#include <memory>
#include <algorithm>
#include <opencv2/opencv.hpp>

template <class T>
class SharedVector;

template <class T>
class SharedVector {
private:
public:
  std::unique_ptr<T[]> vec;
  T* device_vec;
  uint size;
  size_t byteSize;

  SharedVector(std::initializer_list<T>);
  SharedVector(T*, int);
  SharedVector(int);

  void print();

  void sendToGPU();
  void getFromGPU();

  ~SharedVector();
};

#endif
