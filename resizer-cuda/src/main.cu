#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include "SharedVector.cuh"

#include <iostream>
#include <cmath>
#include <chrono>

#include <opencv2/opencv.hpp>

struct Pixel {
    uchar r;
    uchar g;
    uchar b;
};
typedef struct Pixel Pixel;

__global__ void scaleImage(Pixel* input, int inputWidth, int inputHeight, Pixel* output, int outputWidth, int outputHeight) {
    int x = blockIdx.x * blockDim.x + threadIdx.x;
    int y = blockIdx.y * blockDim.y + threadIdx.y;

    if (x >= outputWidth || y >= outputHeight) return;

    float inputScaleX = float(inputWidth) / outputWidth;
    float inputScaleY = float(inputHeight) / outputHeight;

    float inputX = x * inputScaleX;
    float inputY = y * inputScaleY;

    int x0 = floor(inputX);
    int x1 = min(x0 + 1, inputWidth - 1);
    int y0 = floor(inputY);
    int y1 = min(y0 + 1, inputHeight - 1);

    float xLerp = inputX - x0;
    float yLerp = inputY - y0;

    Pixel topLeft = input[y0 * inputWidth + x0];
    Pixel topRight = input[y0 * inputWidth + x1];
    Pixel bottomLeft = input[y1 * inputWidth + x0];
    Pixel bottomRight = input[y1 * inputWidth + x1];

    // Bilinear interpolation
    Pixel outputPixel;
    outputPixel.r = (1 - yLerp) * ((1 - xLerp) * topLeft.r + xLerp * topRight.r) + yLerp * ((1 - xLerp) * bottomLeft.r + xLerp * bottomRight.r);
    outputPixel.g = (1 - yLerp) * ((1 - xLerp) * topLeft.g + xLerp * topRight.g) + yLerp * ((1 - xLerp) * bottomLeft.g + xLerp * bottomRight.g);
    outputPixel.b = (1 - yLerp) * ((1 - xLerp) * topLeft.b + xLerp * topRight.b) + yLerp * ((1 - xLerp) * bottomLeft.b + xLerp * bottomRight.b);

    output[y * outputWidth + x] = outputPixel;
}

Pixel* allocatePixelArray(int width, int height) {
    Pixel* array = (Pixel*) malloc(width * height * sizeof(Pixel));
    return array;
}

void freePixelArray(Pixel* array) {
    free(array);
}

cv::Mat cvMatFromPixelArray(Pixel* pixels, uint width, uint height) {
    cv::Mat mat = cv::Mat(height, width, CV_8UC3);
    for (int y=0; y<height; ++y) {
        for (int x=0; x<width; ++x) {
            uchar* pixel = mat.ptr(y, x);
            pixel[0] = pixels[y*width + x].r;
            pixel[1] = pixels[y*width + x].g;
            pixel[2] = pixels[y*width + x].b;
        }
    }
    return mat;
}

int main(int argc, char** argv) {
    if (argc < 4) {
        std::cout<<"Usage: resizer <imgpath> <newWidth> <newHeight>\n";
        return 0;
    }
    std::string imagePath = std::string(argv[1]);
    uint outputWidthArg = strtol(argv[2], nullptr, 0);
    uint outputHeightArg = strtol(argv[3], nullptr, 0);
    if (outputWidthArg < 1 || outputHeightArg < 1) {
        std::cerr<<"New dimentions need to be positive numbers\n";
        return 1;
    }
    if (outputWidthArg > static_cast<long>(std::numeric_limits<uint>::max()) &&
        outputHeightArg > static_cast<long>(std::numeric_limits<uint>::max())) {
        std::cerr<<"New dimentions can't be greater than "<<std::numeric_limits<uint>::max()<<"!\n";
        return 1;
    }

    uint outputWidth = static_cast<uint>(outputWidthArg);
    uint outputHeight = static_cast<uint>(outputHeightArg);

    cv::Mat image = cv::imread(imagePath, cv::IMREAD_COLOR);
    if (image.empty()) {
        std::cerr<<"Failed to open the image: "<<imagePath<<"\n";
        return 1;
    }
    uint imageWidth = image.size().width;
    uint imageHeight = image.size().height;
    Pixel* pixels = allocatePixelArray(imageWidth, imageHeight);

    for (int x=0; x<imageWidth; ++x) {
        for (int y=0; y<imageHeight; ++y) {
            cv::Vec3b pixel = image.at<cv::Vec3b>(y, x);
            pixels[y*imageWidth + x] = { static_cast<uchar>(pixel[0]), static_cast<uchar>(pixel[1]), static_cast<uchar>(pixel[2])};
        }
    }

    dim3 blockSize(16, 16);
    dim3 gridSize((outputWidth + blockSize.x - 1) / blockSize.x, 
              (outputHeight + blockSize.y - 1) / blockSize.y);

    SharedVector<Pixel> sharedPixels(pixels, imageWidth*imageHeight);
    SharedVector<Pixel> outputPixels(outputWidth*outputHeight);

    sharedPixels.sendToGPU();

    auto start = std::chrono::high_resolution_clock::now();

    for (int i=0; i<100; ++i) {
        scaleImage <<< gridSize, blockSize >>>(sharedPixels.device_vec, imageWidth, imageHeight, outputPixels.device_vec, outputWidth, outputHeight);
    }

    cudaDeviceSynchronize();
    
    auto end = std::chrono::high_resolution_clock::now();

    std::cout<<std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count() << "ms\n";

    //outputPixels.getFromGPU();

    //cv::Mat output = cvMatFromPixelArray(outputPixels.vec.get(), outputWidth, outputHeight);

    freePixelArray(pixels);

    //cv::imwrite("output.jpg", output);
    return 0;
}