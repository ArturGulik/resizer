# Resizer

An OpenMP implementation of image resizing using bilinear interpolation.

# Compilation

```
make all
```

# Usage

For the OpenMP implementation run:

```
./resizer <imgpath> <newWidth> <newHeight>
```

For a single-threaded C++ program run:

```
./resizer-sync <imgpath> <newWidth> <newHeight>
```