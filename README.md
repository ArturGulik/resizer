# Resizer

A CUDA and OpenMP implementation of image resizing using bilinear interpolation.

# Compilation & Usage

Please refer to instructions provided in README files in `resizer-cuda` and `resizer-openmp` directories.
