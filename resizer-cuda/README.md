# Resizer

A CUDA implementation of image resizing using bilinear interpolation.

# Compilation

```
make resizer
```

# Usage

```
resizer <imgpath> <newWidth> <newHeight>
```